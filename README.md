Nunca mais
==========

* Dormir menos de 5h.
* Dormir mais de 8 horas.
* Comer até não aguentar mais.
* Aceitar responsabilidades sem pensar no futuro.
* Cobrar barato pra ficar até tarde trabalhando.
* Usar um editor WYSIWYG por opção para desenvolver projectos.
* Fazer um Projecto usando o Bloco de notas.
* Decorar codigos.
* Ficar mais de 3 dias sem codar.
* Passar uma semana sem atualizar os skills.
